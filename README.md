# HoopaSDK

## Installation

HoopaSDK is available through Hoopa's Bitbucket repository. To install
it, simply add the following line to your Podfile:

```ruby
pod "HoopaSDK", :git => 'https:/bitbucket.org/hoopa/release.git'
```

## Author

Hoopa, 2016. All right reserved.

## License

Hoopa is distributed under a commercial license. See LICENSE file for more info.